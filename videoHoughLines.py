# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 09:49:20 2019

@author: pjshzs
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 10:57:11 2019

@author: pjshzs
"""

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import cv2
import math
def region_of_interest(img, vertices):
    mask = np.zeros_like(img)
    match_mask_color = 255
    cv2.fillPoly(mask, vertices, match_mask_color)
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image
def draw_lines(img, lines, color=[255, 0, 0], thickness=3):
    line_img = np.zeros(
        (
            img.shape[0],
            img.shape[1],
            3
        ),
        dtype=np.uint8
    )
    img = np.copy(img)
    if lines is None:
        return
    for line in lines:
        for x1, y1, x2, y2 in line:
            cv2.line(line_img, (x1, y1), (x2, y2), color, thickness)
    img = cv2.addWeighted(img, 0.8, line_img, 1.0, 0.0)
    return img
def pipeline(image):
    """
    An image processing pipeline which will output
    an image with the lane lines annotated.
    """
    height = image.shape[0]
    width = image.shape[1]
    print(height)
    region_of_interest_vertices = [
        (0, height),
        (width / 2, height / 2),
        (width, height),
    ]
    gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    cannyed_image = cv2.Canny(gray_image, 150, 200)
 
    cropped_image = region_of_interest(
        cannyed_image,
        np.array(
            [region_of_interest_vertices],
            np.int32
        ),
    )
    minLineLength = 30
    maxLineGap = 5    
 
    lines = cv2.HoughLinesP(
        cropped_image,cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength,maxLineGap)
   
 
    left_line_x = []
    left_line_y = []
    right_line_x = []
    right_line_y = []
    l=0
    r=0
    if lines is None:
        lines=[[[1,2,2,1],
               [2,2,3,3]]]
    for line in lines:
        for x1, y1, x2, y2 in line:
            slope = (y2 - y1) / (x2 - x1)
            print(slope)
            if math.fabs(slope) < 0.5:
                continue
            if slope <= 0:
                l+=1
                left_line_x.extend([x1, x2])
                left_line_y.extend([y1, y2])
            else:
                r+=1
                #print([x1, x2],[y1, y2])
                right_line_x.extend([x1, x2])
                right_line_y.extend([y1, y2])
    min_y = int(image.shape[0] * (3 / 5))
    max_y = int(image.shape[0])
    
    if l==0:
        left_line_x.extend([1, 2])
        left_line_y.extend([2, 1])
    poly_left = np.poly1d(np.polyfit(
        left_line_y,
        left_line_x,
        deg=1
    ))
 
    left_x_start = int(poly_left(max_y))
    left_x_end = int(poly_left(min_y))
    if r==0:
        right_line_x.extend([x1, x2])
        right_line_y.extend([y1, y2])
        
    poly_right = np.poly1d(np.polyfit(
        right_line_y,
        right_line_x,
       deg=1
    ))
 
    right_x_start = int(poly_right(max_y))
    right_x_end = int(poly_right(min_y))
    line_image = draw_lines(
        image,
        [[
            [left_x_start, max_y, left_x_end, min_y],
            [right_x_start, max_y, right_x_end, min_y],
        ]],
        thickness=5,
    )
    return line_image
cap = cv2.VideoCapture('Freeway.mp4')
while(cap.isOpened()):
    ret, frame = cap.read()
    output=pipeline(frame)     
    cv2.imshow('frame',output)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
"""
inputImage = cv2.imread("linesTest2.png")
height = inputImage.shape[0]
width = inputImage.shape[1]
region_of_interest_vertices = [
(0, height),
(width / 2, height / 2),
(width, height),]

crop=region_of_interest(
        inputImage,
        np.array(
            [region_of_interest_vertices],
            np.int32
        ),
    )
output=pipeline(inputImage)
plt.imshow(output)

"""
