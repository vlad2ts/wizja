# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 15:07:02 2019

@author: pjshzs
"""
import cv2
import numpy as np
import matplotlib.pyplot as plt
inputImage = cv2.imread("road3.jpg")
x=int(np.size(inputImage,0))
y=int(np.size(inputImage,1))
outputImage =np.zeros([x,y,3])
inputImageGray = cv2.cvtColor(inputImage, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(inputImageGray,150,200,apertureSize = 3)
minLineLength = 30
maxLineGap = 5
lines = cv2.HoughLinesP(edges,cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength,maxLineGap)

for x in range(0, len(lines)):
    for x1,y1,x2,y2 in lines[x]:
        slope = (y2 - y1) / (x2 - x1)
        print(slope)
        #cv2.line(inputImage,(x1,y1),(x2,y2),(0,128,0),2, cv2.LINE_AA)
        pts = np.array([[x1, y1 ], [x2 , y2]], np.int32)
        cv2.polylines(inputImage, [pts], True, (0,0,255),2)

font = cv2.FONT_HERSHEY_SIMPLEX
#cv2.putText(outputImage,"Tracks Detected", (500, 250), font, 0.5, 255)
plt.imshow(cv2.cvtColor(inputImage, cv2.COLOR_BGR2RGB))
#cv2.imwrite('houghProb2.png',inputImage)
