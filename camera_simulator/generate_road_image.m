clc; clear all; close all;

% Generates a road image as a set of 3d points, than performs a projection
% from world coordinates to image coordinates. Intrinsic parameters have
% been derived from VASP project camera, extrinsic parameters can be easily
% manipulated to test different camera position and orientation

%% Generate objects in 3d space to be projected - SAE coordinate system
% generate road points
road_length = 80;
road_width = 4;

z=zeros(road_length,2);
x = repmat(1:size(z,1),size(z,2),1)';
y_l = zeros(road_length,1)-road_width/2;
y_r = zeros(road_length,1)+road_width/2;
y=[y_l y_r];

c = x - min(x(:));
c = c./max(c(:));
c = round(255*c) + 1;
cmap = colormap(jet(256));
c = cmap(c,:);
points = [x(:),y(:),z(:),c];

% generate horizon line
x_hor = 100000*ones(50,1);
y_hor = linspace(-100000, 100000, 50)';
z_hor = zeros(50,1);
points_horizon = [x_hor, y_hor, z_hor];

%% Setup intrinsic camera parameters

image_size = [960,1280];

fx = 1172;          % scaled in pixels (focal length in mm / pixel pitch)
fy = 1172;          % scaled in pixels (focal length in mm / pixel pitch)

% center_x0 = 634.5800171;
% center_y0 = 475.3999939;
center_x0 = image_size(2)/2;
center_y0 = image_size(1)/2;

cam = [fx, 0, center_x0; 
        0, fy, center_y0; 
        0, 0, 1];

% lens distortion parameters
radial_distortion_k1 = -0.238311291;
radial_distortion_k2 = 0.0198269671;
radial_distortion_k3 = 0;
tangential_distortion_p1 = 0.000777259760;
tangential_distortion_p2 = -0.000191924497;

dist = [radial_distortion_k1
    radial_distortion_k2
    tangential_distortion_p1
    tangential_distortion_p2
    radial_distortion_k3]';

% dist =[];     % often it is useful to disable distortion

%% Setup extrinsic camera matrix
yaw_angle = 0;
pitch_angle = -7.41; %-7.41
roll_angle = 0;

x_offset = 0;
y_offset = 0;
z_offset = -2.81;

angles = [yaw_angle, pitch_angle, roll_angle];
position = [x_offset;y_offset;z_offset];

%% Calculate camera extrinsic matrix

%Additional angles are used to tansform SAE coordinate system to camera
%coordinate system with z axis pointed in the observation direction, x and
%y axis are oriented as on typical image
angles_cs = [90, 0, 90];

%Camera position in world coorinate system is NOT equivalent to camera
%extrinsic parameter matrix. Their relationship is descibed by the
%following equation:
% | R   t |   | R_c   C |^(-1)
% |       | = |         |
% | 0   1 |   | 0     1 |
% which after a few steps ends up being:
% R = (R_c)'
% t = -R * C
% where: C - column vector describing the location of the camera-center 
% in world coordinates
% R_c - rotation matrix describing the camera's orientation with respect 
% to the world coordinate axes
tform = eye(4);
% Combine two rotations: SAE to camera coordinate system and camera yaw,
% pitch and roll
tform(1:3,1:3) = (angle2dcm(angles)*angle2dcm(angles_cs))';
tform(1:3,4) = (-1)*tform(1:3,1:3)*position;

%% Project the points into image coordinates
[projected, valid] = project_points(points, cam, tform, dist, image_size,false);
[projected_horizon, valid_horizon] = project_points(points_horizon, cam, tform, dist, image_size,false);
projected = projected(valid,:);
projected_horizon = projected_horizon(valid_horizon,:);
projected_horizon = sortrows(projected_horizon);

%% Display the 3D view and resultant camera image
subplot(1,2,1);
scatter3(points(:,1),points(:,2),points(:,3),20,points(:,4:6),'fill');
hold on

c_system = [0 0 0; 5 0 0; 0 5 0; 0 0 5]'; % 4 points determining the coordinate system
                                          % [Ox Oy Oz; Xx Xy Xz; Yx Yy Yz; Zx Zy Zz]                                       
c_system_rotated = angle2dcm(angles)*angle2dcm(angles_cs)*c_system + ...
    repmat(position,1,4);
display_coordinate_system(c_system);
display_coordinate_system(c_system_rotated);    

axis equal;
set(gca, 'ZDir', 'reverse')
set(gca, 'YDir', 'reverse')
title('Original Points - SAE coordinate system');
xlabel('x')
ylabel('y')
zlabel('z')

subplot(1,2,2);
scatter(projected(:,1),projected(:,2),20,projected(:,3:5),'fill');
hold on;
plot(projected_horizon(:,1),projected_horizon(:,2),'k--')
axis equal;
axis([0 image_size(2) 0 image_size(1)])
axis ij
title('Points projected with camera model');
set(gcf, 'Position', get(0, 'Screensize')); % show plot fullscreen