function [dcm] = angle2dcm(r,seq)
% dcm = angle2dcm(r1,r2,r3,seq)
% builds euler angle rotation matrix
% 
% r = [r1 r2 r3]
% seq = 'ZYpXpp' (default)
%       'ZXZ'
% dcm = direction cosine matrix

if nargin == 1
  seq = 'ZYpXpp';
end

switch seq
  case 'ZYpXpp'
    dcm = Tz(r(1))*Ty(r(2))*Tx(r(3));
  case 'ZXZ'
    dcm = Tz(r(3))*Tx(r(2))*Tz(r(1)); 
end

function A = Tx(a)
% A = [1 0 0;0 cosd(a) sind(a);0 -sind(a) cosd(a)];
A = [1 0 0;0 cosd(a) -sind(a);0 sind(a) cosd(a)];

function A = Ty(a)
% A = [cosd(a) 0 -sind(a);0 1 0;sind(a) 0 cosd(a)];
A = [cosd(a) 0 sind(a);0 1 0;-sind(a) 0 cosd(a)];

function A = Tz(a)
% A = [cosd(a) sind(a) 0;-sind(a) cosd(a) 0;0 0 1];
A = [cosd(a) -sind(a) 0;sind(a) cosd(a) 0;0 0 1];