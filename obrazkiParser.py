# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 10:48:01 2019
Copying png images with specified size to separate folder  
@author: pjshzs
"""

import os
import shutil
import argparse
import cv2
import numpy as np

parser=argparse.ArgumentParser()
parser.add_argument('-i','--input',help='Source directory')
parser.add_argument('-mnh','--minh',help='Min height')
parser.add_argument('-mxh','--maxh',help='Max height')
parser.add_argument('-mnw','--minw',help='Min width')
parser.add_argument('-mxw','--maxw',help='Max width')
parser.add_argument('-clr','--colour',help='Colour: "grey" or "rgb"')


args=parser.parse_args()
dirFolder=args.input
maxHeight=int(args.maxh)
maxWidth=int(args.maxw)
minHeight=int(args.minh)
minWidth=int(args.minw)
colour=args.colour;
if colour=='rgb':
    depth=3
else:
    depth=1    
assert colour in ['grey','rgb'], "Colour is wrong, please choose: 'grey' or 'rgb'"
folderName=('Sorted'+args.minh+'x'+args.minw+'_'+args.maxh+'x'+args.maxw+'_'+colour)

for root, dirs, files in os.walk(dirFolder):
    path = root.split(os.sep)
    #print((len(path) - 1) * '---', os.path.basename(root))
    destFolder = os.path.join(root,folderName)
   
    if not 'Sorted' in root and not os.path.isdir(destFolder) :
        print (root)
        
        os.makedirs(destFolder)
        exist=False
        for file in files:
            
            #print(  os.path.basename(root)) 
            if file.endswith('.png'):
                
                filedir=os.path.join(root,file)
                img=cv2.imread(filedir)
                
                (h,w,bpp) = np.shape(img)
                
                if minHeight<h<maxHeight and minWidth<w<maxWidth and bpp == depth  :
                    shutil.copy2(filedir,destFolder)
                    exist=True
        
        if not exist:
            os.rmdir(destFolder)
       
            

        
