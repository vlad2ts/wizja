# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 12:56:56 2019

@author: pjshzs
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
path='road5edges.jpg'
img = cv2.imread(path)
rows,cols,ch = img.shape
pts1 = np.float32([[135,200],[485,200],[7,320],[600,320]])
pts2 = np.float32([[0,0],[600,0],[0,200],[600,200]])
M = cv2.getPerspectiveTransform(pts1,pts2)
dst = cv2.warpPerspective(img,M,(600,200))
plt.subplot(121),plt.imshow(img),plt.title('Input')
plt.subplot(122),plt.imshow(dst),plt.title('Output')
plt.show()