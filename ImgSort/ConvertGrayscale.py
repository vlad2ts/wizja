# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 12:55:28 2019
Converts image of defined size from rgb to grayscale
 in new folder in defined directory based on ImgColourSort 
@author: pjshzs
"""


import os
import argparse
import cv2
import numpy as np

parser=argparse.ArgumentParser()
parser.add_argument('-i','--input',help='Source directory')
parser.add_argument('-mnh','--minh',help='Min height')
parser.add_argument('-mxh','--maxh',help='Max height')
parser.add_argument('-mnw','--minw',help='Min width')
parser.add_argument('-mxw','--maxw',help='Max width')


args=parser.parse_args()
dirFolder=args.input
maxHeight=int(args.maxh)
maxWidth=int(args.maxw)
minHeight=int(args.minh)
minWidth=int(args.minw)
   
folderName=('Grayscale'+args.minh+'x'+args.minw+'_'+args.maxh+'x'+args.maxw)

for root, dirs, files in os.walk(dirFolder):
    path = root.split(os.sep)
    #print((len(path) - 1) * '---', os.path.basename(root))
    destFolder = os.path.join(root,folderName)
   
    if not 'Grayscale' in root and not os.path.isdir(destFolder) :
        print (root)
        
        os.makedirs(destFolder)
        exist=False
        for file in files:
            
            #print(  os.path.basename(root)) 
            if file.endswith('.png'):
                
                filedir=os.path.join(root,file)
                img=cv2.imread(filedir)
                
                (h,w,bpp) = np.shape(img)
                
                if minHeight<h<maxHeight and minWidth<w<maxWidth and bpp == 3  :
                    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    file2=('gray_'+file)
                    newname=os.path.join(destFolder,file2)
                    cv2.imwrite(newname,gray_img)
                    
                    exist=True
        
        if not exist:
            os.rmdir(destFolder)