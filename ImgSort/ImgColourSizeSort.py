# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 10:48:01 2019
Copying png images of type:.jpg, .png with specified size to separate folder  
if directory is named "Sorted" it goes to next directory 
 
@author: pjshzs
"""

import os
import shutil
import argparse
import cv2
import numpy as np

parser=argparse.ArgumentParser()
parser.add_argument('-i','--input',help='Source directory')
parser.add_argument('-mnh','--minh',help='Min height')
parser.add_argument('-mxh','--maxh',help='Max height')
parser.add_argument('-mnw','--minw',help='Min width')
parser.add_argument('-mxw','--maxw',help='Max width')
parser.add_argument('-clr','--colour',help='Colour: "gray" , "rgb" or "all"')
#assign input arguments
args=parser.parse_args()
dirFolder=args.input
maxHeight=int(args.maxh)
maxWidth=int(args.maxw)
minHeight=int(args.minh)
minWidth=int(args.minw)
colour=args.colour;
 #assertion
assert colour in ['gray','rgb','all'], "Colour is wrong, please choose: 'gray' ,'rgb' or 'all'"
folderName=('Sorted'+args.minh+'x'+args.minw+'_'+args.maxh+'x'+args.maxw+'_'+colour)
w, h = 2, 2;
 
#walking through directories
for root, dirs, files in os.walk(dirFolder):
    path = root.split(os.sep)
    destFolder = os.path.join(root,folderName)
   #check if directory exist creating number
    if  os.path.isdir(destFolder) :
        num=0
        folder=destFolder
        while os.path.isdir(destFolder):
            num=num+1
            destFolder=folder+str(num)
        #doesn't sort images in already created folders 
    if not 'Sorted' in root:
            
        os.makedirs(destFolder)
        exist=False
        if colour=="all":
            for file in files:
                if file.endswith('.png') or file.endswith('.jpg') :
                
                    filedir=os.path.join(root,file)
                    img=cv2.imread(filedir)
                    (h,w,bpp) = np.shape(img)
                    exist=True
                    if minHeight<h<maxHeight and minWidth<w<maxWidth:
                        shutil.copy2(filedir,destFolder)
        
            if not exist:
                os.rmdir(destFolder)
        
        else:
            for file in files:
                if file.endswith('.png') or file.endswith('.jpg'):
                
                    filedir=os.path.join(root,file)
                    img=cv2.imread(filedir)
                    (h,w,bpp) = np.shape(img)
                    if np.array_equal(img[:,:,0],img[:,:,1]) :
                        imgColour='gray' 
                    else:
                        imgColour='rgb'
                    exist=True
                    if minHeight<h<maxHeight and minWidth<w<maxWidth and imgColour==colour :
                        shutil.copy2(filedir,destFolder)
        
            if not exist:
                os.rmdir(destFolder)
       
            

        
