# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 13:18:46 2019

@author: pjshzs
"""

import numpy as np
import cv2
import math as m
import time
cap = cv2.VideoCapture("Freeway.mp4")

def region_of_interest(img, vertices):
    mask = np.zeros_like(img)
    match_mask_color = 255 # <-- This line altered for grayscale.
    
    cv2.fillPoly(mask, vertices, match_mask_color)
    masked_image = cv2.bitwise_and(img, mask)
    return masked_image
height=480
width=960
region_of_interest_vertices = [
    (0, height),
    (width / 2, height / 2),
    (width, height),
]
while(cap.isOpened()):
    ret, frame = cap.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray,100,200)
    cropped_edges = region_of_interest(
    edges,
    np.array([region_of_interest_vertices], np.int32),
)
    minLineLength = 100
    maxLineGap = 10
    lines = cv2.HoughLinesP(cropped_edges,cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength,maxLineGap)
    if  np.any(lines):
        for x in range(0, len(lines)):
            for x1,y1,x2,y2 in lines[x]:
                a=abs(x2-x1)
                b=abs(y2-y1)
                alpha=m.atan(b/a)
                if alpha > 0.26 :
                    pts = np.array([[x1, y1 ], [x2 , y2]], np.int32)
                    cv2.polylines(frame, [pts], True, (0,0,255),2)

    cv2.imshow('frame',frame)
    time.sleep(1/30)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()