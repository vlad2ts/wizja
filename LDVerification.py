# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 11:10:34 2019

@author: pjshzs
"""

#Vanishing Point Perspective - 101computing.net/vanishing-point-perspective/
import turtle
import tkinter as tk
class DrawMesh(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)
        self.canvas=tk.Canvas(self,width=500,height=500)
        self.canvas.pack(expand=True)
        self.canvas.bind_all("<Button-1>",self.clickleft)
        self.screen=turtle.TurtleScreen(self.canvas)
        
        self.t=turtle.RawTurtle(self.screen)
        self.t.hideturtle()
        self.t.onclick #(self.clickleft,2)
        self.t.color("purple")
        #myPen.tracer(0)
        self.t.speed(0)
        self.x1=0
        self.y1=0
        self.x2=0
        self.y2=0
        self.draw_test_line=True
        self.x_vanishingPoint=0
        self.y_vanishingPoint=152
        self.y_horizon=-50
        self.xBottomLeft = -400
        self.yBottomLeft = -300
        self.xBottomRight = 400
        self.yBottomRight = -300
        self.xTopLeft=-1 #Will be calculated later on in the program
        self.yTopLeft=self.y_horizon
        self.xTopRight=-1 #Will be calculated later on in the program
        self.yTopRight=self.y_horizon
        self.ratio = 0.8
        self.coordlist=[]
        self.numberOfCols = 5
        self.numberOfRows = 12
        self.rowHeight = 40
        self.a1=0
        self.a2=0
        self.b1=0
        self.b2=0

#Draw Vertical Vanishing Lines

    def Draw_lines(self):
        self.t.color("purple")
        def Draw_vertical_lines():
            self.coordlist.clear()
            for i in range(0,self.numberOfCols+1):
                self.xFrom=self.xBottomLeft +(self.xBottomRight - self.xBottomLeft)/self.numberOfCols*i
                self.yFrom=self.yBottomLeft
                #Line Equation: y=ax+b
                if self.xFrom!=self.x_vanishingPoint:
                    self.a=(self.yFrom-self.y_vanishingPoint)/(self.xFrom-self.x_vanishingPoint)
                    self.b=self.y_vanishingPoint - (self.a*self.x_vanishingPoint)
                    self.x_horizon = (self.y_horizon - self.b)/self.a
                else:
                    self.x_horizon = self.xFrom
                self.t.penup()
                self.t.goto(self.xFrom,self.yFrom)
                self.t.pendown()
                self.t.goto(self.x_horizon,self.y_horizon)
                self.coordlist.append([self.xFrom,self.yFrom,self.x_horizon,self.y_horizon])
                if i==0:
                    self.xTopLeft = self.x_horizon
                    self.a1=self.a
                    self.b1=self.b
                elif i==self.numberOfCols:
                    self.xTopRight= self.x_horizon
                    self.a2=self.a
                    self.b2=self.b
   

        #Draw Horizontal Lines
        def Draw_horizontal_lines():
            self.yFrom=self.yBottomLeft
            self.yTo=self.yBottomLeft
            for i in range(0,self.numberOfRows):
                self.xFrom=(self.yFrom-self.b1)/self.a1
                self.xTo=(self.yTo-self.b2)/self.a2
                self.t.penup()
                self.t.goto(self.xFrom,self.yFrom)
                self.t.pendown()
                self.t.goto(self.xTo,self.yTo) 
                self.yFrom+=self.rowHeight
                self.yTo+=self.rowHeight
                self.rowHeight*=self.ratio

        Draw_vertical_lines()
        self.numberOfRows+=1 #Number of lines to draw
        self.ratio = 0.8
        self.rowHeight = (self.y_horizon-self.yBottomLeft)*(self.ratio-1)/((self.ratio**(self.numberOfRows-1))-1)
    # Draw_horizontal_lines()
        self.screen._update()
    def draw_test_lines(self):
        self.t.color("green")
        self.t.penup()
        self.t.goto(self.x1,self.y1)
        self.t.pendown()
        self.t.goto(self.x2,self.y2)
        self.t.penup()

    def evaluate_criterium(self):
        def solve_line(x1,y1,x2,y2):
            a = (y2-y1)/(x2-x1)
            b = y1-a*x1
            return (a,b)
        def crosspoint(a1,b1,a2,b2):
            xcross=(b1-b2)/(a2-a1)
            ycross=(a2*b1-b2*a1)/(a2-a1)
            return(xcross,ycross)  
        crossing_points=[]      
        test_line=solve_line(self.x1,self.y1,self.x2,self.y2)
        for lines in self.coordlist:
            line=solve_line(lines[0],lines[1],lines[2],lines[3])
            crossing_points.append(crosspoint(test_line[0],test_line[1],line[0],line[1]))
        print("crossing",crossing_points)
        
    def Mesh_reset(self):
        self.x_vanishingPoint,self.y_vanishingPoint
        self.xTopLeft=-1 #Will be calculated later on in the program
        self.yTopLeft=self.y_horizon
        self.xTopRight=-1 #Will be calculated later on in the program
        self.yTopRight=self.y_horizon
        self.ratio = 0.8
        self.numberOfCols = 5
        self.numberOfRows = 12
        self.rowHeight = 40
        self.a1=0
        self.a2=0
        self.b1=0
        self.b2=0
    def clickleft(self,event):
        self.t.clear()
        #self.Mesh_reset()
        #self.x_vanishingPoint=event.x-250
        #self.y_vanishingPoint=250-event.y
        if self.draw_test_line==True:
            self.x1=event.x-250
            self.y1=250-event.y
            self.draw_test_line=False
        else:
            self.x2=event.x-250
            self.y2=250-event.y
            self.draw_test_line=True
        if self.draw_test_line==True:
            self.draw_test_lines()
            self.evaluate_criterium()
        self.Draw_lines()
        print(self.draw_test_line,self.x1,self.y1,self.x2,self.y2)
        

vanishing_point=[[0,150],[0,50],[50,0],[50,50],[-50,50],[50,-50]]

root=tk.Tk()
root.geometry('500x500')
root.resizable(0,0)
root.wm_title("Vanishing Point")
mesh=DrawMesh(root,bg='blue')
mesh.pack()
mesh.Draw_lines()
root.mainloop()

#mesh.screen.mainloop()

#myPen.shape("arrow")

#for pos in vanishing_point:
"""while True: 
  mesh.Mesh_reset()  
  #mesh.x_vanishingPoint=pos[0]
  #mesh.y_vanishingPoint=pos[1]
  mesh.Draw_lines()
  mesh.getscreen().update()"""