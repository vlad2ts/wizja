# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 14:20:45 2019

@author: pjshzs
"""

import cv2
import numpy as np

img = cv2.imread('road5.jpg')
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#ret,bw = cv2.threshold(gray,127,255,cv2.THRESH_BINARY)
#edges1 = cv2.Canny(bw,50,150,apertureSize = 3)
edges = cv2.Canny(gray,50,150,apertureSize = 3)
minLineLength = 50
maxLineGap = 30

lines = cv2.HoughLinesP(edges,1,np.pi/180,200)
print(np.size(lines,0))
for num in range(50):

    for x1,y1,x2,y2 in lines[num]:
    
        cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)

cv2.imwrite('road5linesp.jpg',img)
