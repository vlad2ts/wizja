# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 12:56:56 2019

@author: pjshzs
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
path='road5edges.jpg'
img = cv2.imread(path)
rows,cols,ch = img.shape
pts1 = np.float32([[423,310],[491,309],[147,477],[765,450]])
pts2 = np.float32([[0,0],[300,0],[0,300],[300,300]])
M = cv2.getPerspectiveTransform(pts1,pts2)
dst = cv2.warpPerspective(img,M,(300,300))
plt.subplot(121),plt.imshow(img),plt.title('Input')
plt.subplot(122),plt.imshow(dst),plt.title('Output')
plt.show()