# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 11:26:41 2019
y
@author: pjshzs
"""
import cv2
from curves import Curves
from helpers import roi
from birdseye import BirdsEye
#import matplotlib.pyplot as plt
import pickle
import numpy as np
from lanefilter import LaneFilter
with open('calibration_data.p', 'rb') as f:
    x = pickle.load(f)
#raw_image=cv2.imread('framecut.jpg')
matrix = x['camera_matrix']
distortion_coef = x['distortion_coefficient']

p = { 'sat_thresh': 120, 'light_thresh': 40, 'light_thresh_agr': 205,
'grad_thresh': (0.7, 1.4), 'mag_thresh': 40, 'x_thresh': 20 }

laneFilter = LaneFilter(p)
#source_points = [(580, 460), (205, 720), (1110, 720), (703, 460)]
#destination_points = [(320, 0), (320, 720), (960, 720), (960, 0)]
source_points = [(435, 345), (154, 540), (832, 540), (527, 345)]
destination_points = [(240, 0), (240, 540), (720, 540), (720, 0)]

birdsEye = BirdsEye(source_points, destination_points, 
                    matrix, distortion_coef)

curves = Curves(number_of_windows = 9, margin = 100, minimum_pixels = 50, 
                ym_per_pix = 30 / 720 , xm_per_pix = 3.7 / 700)


#sky_view_image = birdsEye.sky_view(raw_image)


#

#cv2.imwrite('perspektywa.jpg',sky_view_image)
#cv2.imwrite('wynik.jpg',result['image'])
#cv2.imwrite('wynik_projection.jpg',ground_img_with_projection)
cap = cv2.VideoCapture('Freeway.mp4')
k=0
while(cap.isOpened()):
    ret,raw_image = cap.read()
    k+=1
    if k>5:
    
        undistorted_image = birdsEye.undistort(raw_image)
        binary = LaneFilter.apply(laneFilter,undistorted_image)
        lane_pixels = np.logical_and(birdsEye.sky_view(binary), roi(binary)).astype(np.uint8)
        result = Curves.fit(curves,lane_pixels)
        ground_img_with_projection = birdsEye.project(undistorted_image, binary,
        result['pixel_left_best_fit_curve'], result['pixel_right_best_fit_curve'])
        
        cv2.imshow('frame',ground_img_with_projection)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()