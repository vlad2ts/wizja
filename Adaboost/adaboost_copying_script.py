# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 09:55:26 2018

Kod który kopiuje plik adaboost_numer.cpp 
do folderu docelowego z najnowszego folderu "fine_calib"
i definiuje w pliku detector table.h 
nowy Cascade id i przypisuje numer adaboost do R CR CNR

@author: pjshzs Vladyslav Tsal-Tsalko
"""

import os
import shutil
import argparse
#User input

parser=argparse.ArgumentParser()
parser.add_argument('-i','--input',help='Source directory (TSR )')
parser.add_argument('-o','--output',help='Destination directory (REPO/utilities/trained_code) ')
parser.add_argument('-t','--type',help='Typ znaku: R,CR,CNR')
args=parser.parse_args()
dirFolder=args.input
destFolder=args.output
typeName=args.type

assert typeName in ['R','CR','CNR'], "The type is wrong, please choose: 'R,CR,CNR'"

#searching for newest folder
subfolders = [f.path for f in os.scandir(dirFolder) if f.is_dir() if "fine_calib" in f.path]
timestamp = [os.stat(path).st_mtime for path in subfolders]
max_value = max(timestamp)
max_index = timestamp.index(max_value)
#getting adaboost file
filename='b'
for d in os.scandir(subfolders[max_index]):
    if d.is_file() and "adaboost_const_" in d.name and d.name.endswith('.cpp') :
        filedir=d.path
        filename=d.name
#getting adaboost number
assert filename  !='b',"File with .cpp extension was not found!"        
words=filename.split('_')
number=words[2]
#copying adaboost
destFolder=os.path.join(destFolder,'utilities','trained_code')
shutil.copy2(filedir,destFolder)
#rreading detector_table.h
file = open(os.path.join(destFolder,'detector_table.h'),'r')
oldfile=open(os.path.join(destFolder,'detector_table_old.h'),'w+')
textlines=file.readlines()
text=''.join(textlines)
file.close()
oldfile.write(text)

oldfile.close()

#First line
lookup='#ifdef ENABLE_DETECTOR_TSR'
for num, line in enumerate(textlines):
    
    if lookup in line:
            firstline=num+1
#print (content.index('#CASCADE_ID_05'))
b=True
num=1
key='CASCADE_ID_X'
stopkey='#else'
frase=key+str(num)
HashKey='CASCADE_HASH_TSR_'
cascadeHash=HashKey+typeName
defined=False
posID=0
posHash=0
numline=firstline
#Searching positions of text
for i,line in enumerate(textlines[firstline:],firstline):
    if number in line:
        defined=True
        
    if cascadeHash in line:
        if defined==False:        
            posHash=numline+1
        else:
            posHash=numline
    
    if frase in line and defined==False and HashKey not in line:
            posID=numline+1
            num=num+1
            frase=key+str(num)
                       
    elif stopkey in line:
        b=False
        break
    numline=numline+1
    
if posID==0:
    posID=firstline
    
if defined==False:
    
    frase2='#define '+frase+' '+number+'\n'    
    textlines.insert(posID,frase2) 
# making text changes
words=textlines[posHash].split(' ')  
textlines[posHash]=words[0]+' '+words[1]+' '+frase+'\n'
output=''.join(textlines) 
#writing output
newfile=open(os.path.join(destFolder,'detector_table.h'),'w+')
newfile.write(output)
newfile.close()
