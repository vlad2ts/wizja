# -*- coding: utf-8 -*-
"""
Created on Thu Nov 29 13:51:46 2018

@author: pjshzs
"""

"""
Created on Thu Nov 29 09:55:26 2018

Kod który kopiuje plik adaboost_numer.cpp 
do folderu docelowego z najnowszego folderu "fine_calib"
i definiuje w pliku detector table.h 
nowy Cascade id i przypisuje numer adaboost do R CR CNR

@author: pjshzs Vladyslav Tsal-Tsalko
"""
import os
import shutil
import argparse
#User input
dirFolder=''
destFolder=''
def input():
    parser=argparse.ArgumentParser()
    parser.add_argument('-i','--input',help='Source directory (TSR )')
    parser.add_argument('-o','--output',help='Destination directory (REPO/utilities/trained_code) ')
    parser.add_argument('-t','--type',help='Typ znaku: R,CR,CNR')
    args=parser.parse_args()
    dirFolder=args.input
    destFolder=args.output
    typeName=args.type
    


#searching for newest folder
subfolders = [f.path for f in os.scandir(dirFolder) if f.is_dir() if "fine_calib" in f.path]
timestamp = [os.stat(path).st_mtime for path in subfolders]
print(subfolders)
print(timestamp)
max_value = max(timestamp)
max_index = timestamp.index(max_value)
print(max_index)
#getting adaboost file
for d in os.scandir(subfolders[max_index]):
    
    if d.is_file():
        if "adaboost_const_" in d.name:
            if d.name.endswith('.cpp'):
                filedir=d.path
                filename=d.name
#getting adaboost number
words=filename.split('_')
number=words[2]
print(number)
#copying adaboost
destFolder=os.path.join(destFolder,'utilities','trained_code')
print(destFolder)
print(filedir)
shutil.copy2(filedir,destFolder)
#rreading detector_table.h
file = open(os.path.join(destFolder,'detector_table.h'),'r')
oldfile=open(os.path.join(destFolder,'detector_table_old.h'),'w+')
textlines=file.readlines()
text=''.join(textlines)
file.close()
oldfile.write(text)

oldfile.close()

#First line
lookup='#ifdef ENABLE_DETECTOR_TSR'
for num, line in enumerate(textlines):
    
    if lookup in line:
            print ('found at line:', num)
            firstline=num+1
#print (content.index('#CASCADE_ID_05'))
b=True
num=1
key='CASCADE_ID_X'
stopkey='#else'
frase=key+str(num)
HashKey='CASCADE_HASH_TSR_'
cascadeHash=HashKey+typeName
defined=False
posID=0
posHash=0
numline=firstline
#Searching positions of text
while b:
    if number in textlines[numline]:
        defined=True
    
    if cascadeHash in textlines[numline]:
        if defined==False:        
            posHash=numline+1
        else:
            posHash=numline
    
    if frase in textlines[numline] and defined==False and HashKey not in textlines[numline]  :
            
            posID=numline+1
            num=num+1
            frase=key+str(num)
            print(frase)
                       
    elif stopkey in textlines[numline]:
        b=False
        break
    
        
    numline=numline+1  

if posID==0:
    posID=firstline
    
print(numline)
print(textlines[numline])
print('posid',posID)
if defined==False:
    
    frase2='#define '+frase+' '+number+'\n'    
    textlines.insert(posID,frase2) 
# making text changes
words=textlines[posHash].split(' ')  
textlines[posHash]=words[0]+' '+words[1]+' '+frase+'\n'
output=''.join(textlines)          
#print(output)        
#writing output
newfile=open(os.path.join(destFolder,'detector_table.h'),'w+')
newfile.write(output)
newfile.close()
    
#file.close()
#print(textlines)