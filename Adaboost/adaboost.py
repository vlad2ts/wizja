# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import shutil
#User input
dirFolder=input("Enter source folder: ")
destFolder=input("Enter destination folder: ")
typeName=input("Choose type (R,CR,CNR): " )
#searching for newest folder
subfolders = [f.path for f in os.scandir(dirFolder) if f.is_dir() if "fine_calib" in f.path]
timestamp = [os.stat(path).st_mtime for path in subfolders]
print(subfolders)
print(timestamp)
max_value = max(timestamp)
max_index = timestamp.index(max_value)
print(max_index)
#getting adaboost file
for d in os.scandir(subfolders[max_index]):
    
    if d.is_file():
        if "adaboost_const_" in d.name:
            if d.name.endswith('.cpp'):
                filedir=d.path
                filename=d.name
#getting adaboost number
words=filename.split('_')
number=words[2]
print(number)
#copying adaboost
destFolder=os.path.join(destFolder,'utilities','trained_code')
print(destFolder)
print(filedir)
shutil.copy2(filedir,destFolder)

file = open(os.path.join(destFolder,'detector_table.h'),'r')
oldfile=open(os.path.join(destFolder,'detector_table_old.h'),'w+')
textlines=file.readlines()
text=''.join(textlines)
file.close()
oldfile.write(text)

oldfile.close()


lookup='#ifdef ENABLE_DETECTOR_TSR'
for num, line in enumerate(textlines):
    
    if lookup in line:
            print ('found at line:', num)
            numline=num+1
#print (content.index('#CASCADE_ID_05'))
b=True
num=1
key='CASCADE_ID_X'
stopkey='#else'
frase=key+str(num)
cascadeHash='CASCADE_HASH_TSR_'+typeName
defined=False
while b:
    if number in textlines[numline]:
        defined=True
    elif frase in textlines[numline]:
        if defined==False:
        
            posID=numline+1
            num=num+1
            frase=key+str(num)
            print(frase)
    elif cascadeHash in textlines[numline]:
        if defined==False:        
            posHash=numline+1
        else:
            posHash=numline
                
    elif stopkey in textlines[numline]:
        b=False
        break
    
        
    numline=numline+1  


print(numline)
print(textlines[numline])
if defined==False:

    frase2='#define '+frase+' '+number+'\n'    
    textlines.insert(posID,frase2) 

words=textlines[posHash].split(' ')  
textlines[posHash]=words[0]+' '+words[1]+' '+frase+'\n'
output=''.join(textlines)          
#print(output)        
newfile=open(os.path.join(destFolder,'detector_table.h'),'w+')
newfile.write(output)
newfile.close()
    
#file.close()
#print(textlines)