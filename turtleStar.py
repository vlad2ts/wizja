# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 12:25:28 2019

@author: pjshzs
"""

from turtle import *
color('red', 'yellow')
begin_fill()
while True:
    forward(200)
    left(170)
    if abs(pos()) < 1:
        break
end_fill()
done()