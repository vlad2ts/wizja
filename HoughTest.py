# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 09:28:11 2019

@author: pjshzs
"""

import cv2
import numpy as np
path='road1.png'
img = cv2.imread(path)
img_copy=cv2.imread(path)

Ysize=int(np.size(img,0))
Xsize=int(np.size(img,1))
Yhalf=int(Ysize/2)

gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
ret,bw = cv2.threshold(gray,127,255,cv2.THRESH_BINARY)
edges = cv2.Canny(bw,50,150,apertureSize = 3)
edges1 = cv2.Canny(gray,50,150,apertureSize = 3)
#kernel = np.ones((5,5),np.uint8)
#edgesThick=cv2.dilate(edges1,kernel,1)
#cv2.imshow('img',img_crop)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
lines = cv2.HoughLines(edges,1,np.pi/180,100)
lineLength=1000
print(lines)
for num in range(15):
    for rho,theta in lines[num]:    
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + lineLength*(-b))
        y1 = int(y0 + lineLength*(a))
        x2 = int(x0 - lineLength*(-b))
        y2 = int(y0 - lineLength*(a))
            
        cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
        
img_crop = img[Yhalf:Ysize,0:Xsize]

img_copy[Yhalf:Ysize,0:Xsize]=img_crop
cv2.imshow('img',img_copy)
cv2.waitKey(0)
cv2.destroyAllWindows()
cv2.imwrite('road5edges.jpg',img_copy)