# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 12:35:20 2019

@author: pjshzs
"""

from tkinter import *

master = Tk()

listbox = Listbox(master)
listbox.pack()

listbox.insert(END, "a list entry")

for item in ["one", "two", "three", "four"]:
    listbox.insert(END, item)

mainloop()