# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 12:53:26 2019

@author: pjshzs
"""

from tkinter import Tk
from tkinter.filedialog import askopenfilename

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file
print(filename)