# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 13:41:07 2019

@author: pjshzs
"""

import tkinter as tk
class ScrollableFrame(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)

        # create a canvas object and a vertical scrollbar for scrolling it
        self.vscrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        self.vscrollbar.pack(side='right', fill="y",  expand="false")
        self.canvas = tk.Canvas(self,
                                bg='#444444', bd=0,
                                height=350,
                                highlightthickness=0,
                                yscrollcommand=self.vscrollbar.set)
        self.canvas.pack(side="left", fill="both", expand="true")
        self.vscrollbar.config(command=self.canvas.yview)

        # reset the view
        self.canvas.xview_moveto(0)
        self.canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = tk.Frame(self.canvas, **kwargs)
        self.canvas.create_window(0, 0, window=self.interior, anchor="nw")

        self.bind('<Configure>', self.set_scrollregion)


    def set_scrollregion(self, event=None):
        """ Set the scroll region on the canvas"""
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))
        
def popup_bonus():
    win = tk.Toplevel()
    win.wm_title("Window")

    items=frame.items
    checkbox_pane = ScrollableFrame(win, bg='#444444')
    checkbox_pane.pack(expand="true", fill="both")
    i=0
    for name in items:
        i+=1
        tk.Checkbutton(checkbox_pane.interior, text=name).grid(row=i, column=0)
    

    
class App(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master, height=100, width=100)
        self.entry = tk.Entry(self)
        self.entry.focus()
        self.entry.pack()
        self.clear_button = tk.Button(self, text="Clear text", command=self.add_item)
        self.clear_button.pack()
        self.popup=tk.Button(self,text="Popup",command=popup_bonus)
        self.popup.pack()
        self.items=[('ketchuppp'),('moyyyo')]
        self.remove = tk.Button(self,text = "Remove",command = self.remove_item)
        
    def add_item(self):
        word=self.entry.get()
        self.items.append(word)
        self.entry.delete(0, 'end')
    def remove_item(self):
        print('nic')
        
        

root = tk.Tk()
frame = App(root)
frame.pack(expand=True, fill='both')
root.mainloop()

    