# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 13:51:36 2019

@author: pjshzs
"""

import tkinter as tk   # AFAIK Tkinter is always capitalized
import os
import json
#import pyperclip

from tkinter.filedialog import askopenfilename
#import easygui as eg
class ScrollableFrame(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)

        # create a canvas object and a vertical scrollbar for scrolling it
        self.vscrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        self.vscrollbar.pack(side='right', fill="y",  expand="false")
        self.canvas = tk.Canvas(self,
                                bg='#8c2525', bd=0,
                                height=350,
                                highlightthickness=0,
                                yscrollcommand=self.vscrollbar.set)
        self.canvas.pack(side="left", fill="both", expand="true")
        self.vscrollbar.config(command=self.canvas.yview)

        # reset the view
        self.canvas.xview_moveto(0)
        self.canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = tk.Frame(self.canvas, **kwargs)
        self.canvas.create_window(0, 0, window=self.interior, anchor="nw")

        self.bind('<Configure>', self.set_scrollregion)



        

    def set_scrollregion(self, event=None):
        """ Set the scroll region on the canvas"""
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))
        

class App(tk.Frame):
    characterPrefix = "character_"
    def __init__(self, master, list, name, id):
        tk.Frame.__init__(self, master)

        # character box
        tk.Label(self, text = name).grid(row = 0, column = 0, rowspan = 1, columnspan = 2)
        self.charbox = tk.Listbox(self)  # You'll want to keep this reference as an attribute of the class too.
        self.items=list
        self.idi=id
        for chars in self.items:
            self.charbox.insert('end', chars)
        self.charbox.grid(row = 1, column = 0, rowspan = 5)
        charadd = tk.Button(self, text = "   Add   ", command = self.addchar)
        charremove = tk.Button(self, text = "Remove", command = self.printvars)
        chooselib=tk.Button(self,text="Choose", command=self.checkboxes)
        charadd.grid(row = 1, column = 1)
        charremove.grid(row = 2, column = 1)
        chooselib.grid(row = 3, column = 1)
        self.vars = []

        self.checkboxes_values_dlls = []
        self.checkboxes_values_arguments = []

        

        #SETTINGS
        self.settings = {}
        self.default_settings_filename = 'settings.json'
        
        try:
            with open(self.default_settings_filename, 'r') as fp:
                self.settings = json.load(fp)
        except FileNotFoundError:
            print("Settings file not found - creating new one.")
            self.settings = {'iv_system_path':'', 'dlls': {}, 'arguments': {}}
            with open(self.default_settings_filename, 'w') as fp:
                json.dump(self.settings, fp)


       
    def addchar(self, initialCharacter='', initialInfo=''):
        t = tk.Toplevel(root)  # Creates a new window
        t.title("Add character")
        characterLabel = tk.Label(t, text="Character name:")
        characterEntry = tk.Entry(t)
        characterEntry.insert(0, initialCharacter)
        infoLabel = tk.Label(t, text="Info:")
        infoEntry = tk.Entry(t)
        infoEntry.insert(0, initialInfo)
        def create():
            characterName = characterEntry.get()
            self.charbox.insert("end", characterName)
            self.items.append(characterName)
        #    with open(app.characterPrefix + characterName, 'w') as f:
        #            f.write(infoEntry.get())
            t.destroy()
        createButton = tk.Button(t, text="Create", command=create)
        cancelButton = tk.Button(t, text="Cancel", command=t.destroy)
        

        characterLabel.grid(row=0, column=0)
        infoLabel.grid(row=0, column=1)
        characterEntry.grid(row=1, column=0)
        infoEntry.grid(row=1, column=1)
        createButton.grid(row=2, column=0)
        cancelButton.grid(row=2, column=1)

    def removechar(self):
        for index in self.charbox.curselection():
          #  item = self.charbox.get(int(index))
            self.charbox.delete(int(index))
            self.items.remove(self.items[int(index)])
    def checkboxes(self):
        print("id=", self.idi)
        if(self.idi==1):
            win1 = tk.Toplevel()
            win1.wm_title("Select dlls")
            checkbox_pane1 = ScrollableFrame(win1, bg='#224455')
            checkbox_pane1.pack(expand="true", fill="both")

            print("one")
            for index, names in enumerate(dll_list):
                
                self.checkboxes_values_dlls.append(tk.IntVar())
                tk.Checkbutton(checkbox_pane1.interior, text=names, variable=self.checkboxes_values_dlls[index]).grid(row=index, column=0, sticky="W" )


        elif(self.idi==2):
            win2 = tk.Toplevel()
            win2.wm_title("Select arguments")
            checkbox_pane2 = ScrollableFrame(win2, bg='#000000')
            checkbox_pane2.pack(expand="true", fill="both")
            print("two")
        
            for index, name in enumerate(self.items):
                self.checkboxes_values_arguments.append(tk.IntVar())
                tk.Checkbutton(checkbox_pane2.interior, text=name, variable=self.checkboxes_values_arguments[index]).grid(row=index, column=0, sticky="W")

        else:
            print('ERROR')
    def printvars(self):
        print(self.checkboxes_values_dlls)

   # def editchar(self):
        # You can implement this one ;)
class Fileselector(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        self.select_ivsystem_button = tk.Button(self, text = "Add file", fg = "red", command = self.select_ivsystem_file)# 'fg - foreground' is used to color the contents
        self.select_ivsystem_button.grid(row = 0)
        self.select_ivsystem_label = tk.Label(self,text="Please add IV_SYSTEM file.")
        self.select_ivsystem_label.grid(row = 0,column = 1)

        self.select_dvl_button = tk.Button(self, text = "Add file", fg = "green", command = self.select_dvl_file)# 'text' is used to write the text on the Button
        self.select_dvl_button.grid(row = 1)
        self.select_dvl_label = tk.Label(self,text="Please add .DVL file.")
        self.select_dvl_label.grid(row = 1, column = 1)
    
    def select_ivsystem_file(self):
        global iv_system_file_dir
        iv_system_file_dir = askopenfilename() # show an "Open" dialog box and return the path to the selected file
        print(iv_system_file_dir)
        extension = iv_system_file_dir[-4:]
        iv_directory,iv_filename=os.path.split(iv_system_file_dir)
        global dll_list
        dll_list=list()
        for root, dirs, files in os.walk(iv_directory):
            for file in files:
                if file.endswith(".dll"):
                    dll_list.append(file)
        print(dll_list)


        if extension == '.exe':

            print("This file is executable, OK")
            self.select_ivsystem_label.config(text=iv_system_file_dir)
        else:
            print("It's not an executable file...")
            self.select_ivsystem_label.config(text="Selected file was not an executable, try again.")
            iv_system_filename = ''


    
    def select_dvl_file(self):
        global dvl_filename
        ftypes = [('dvls','*.dvl'),('All files','*')]

        dvl_filename = askopenfilename(filetypes=ftypes) # show an "Open" dialog box and return the path to the selected file
        
        #check_if_file_is_dvl
        extension = dvl_filename[-4:]
        if extension == '.dvl':

            print("This file is dvl, OK")
            self.select_dvl_label.config(text=dvl_filename)
        else:
            print("It's not dvl...")
            self.select_dvl_label.config(text="Selected file was not a dvl file, try again.")
            dvl_filename = ''


def copy_to_clipboard():
    pass


namelib = 'Selected dynamic libraries'
namearg = 'Selected command arguments'     

list_of_dlls = ['config_from_host', ('IFV_PCResim.dll')]
list_of_arguments = [('BATCH'), ('NOKILL')]

root = tk.Tk()
root.wm_title("IA Development Kit")

selectLib = App(root, list_of_dlls, namelib, 1)
selectarg = App(root, list_of_arguments, namearg, 2)

head = Fileselector(root)
head.grid(row=0, column=0, columnspan=2)

selectLib.grid(row=1, column=0)
selectarg.grid(row=1, column=1)

run_button = tk.Button(root, text = "Run", command=selectLib.printvars)
load_task_button = tk.Button(root, text = "Load task", command=selectLib.printvars)
save_task_button = tk.Button(root, text = "Save task", command=selectLib.printvars)
copy_to_clipboard_button = tk.Button(root, text = "Copy command", command=selectLib.printvars)

load_task_button.grid(row = 2, column=0)
save_task_button.grid(row = 2, column=1)
run_button.grid(row = 3, column=2)
copy_to_clipboard_button.grid(row = 3, column=3)

root.mainloop()