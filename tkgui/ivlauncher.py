# -*- coding: utf-8 -*-
"""
Created on Fri Mar  1 12:59:43 2019

@author: pjshzs
"""

import tkinter
from tkinter.filedialog import askopenfilename
window = tkinter.Tk()
window.title("GUI")
def chooseiv():
    global filename1
    filename1 = askopenfilename() # show an "Open" dialog box and return the path to the selected file
    print(filename1)
    label1.config(text=filename1)

def choosedvl():
    global filename2
    filename2 = askopenfilename() # show an "Open" dialog box and return the path to the selected file
    label2.config(text=filename2) 
   

# creating 2 frames TOP and BOTTOM

# now, create some widgets in the top_frame and bottom_frame
btn1 = tkinter.Button(window, text = "Choose IV", fg = "red", command = chooseiv)# 'fg - foreground' is used to color the contents
btn1.grid(row = 0)
label1 = tkinter.Label(window,text="blah")
label1.grid(row = 0,column = 1)
btn2 = tkinter.Button(window, text = "Choose DVL", fg = "green", command = choosedvl)# 'text' is used to write the text on the Button
btn2.grid(row = 1)
label2 = tkinter.Label(window,text="blah")
label2.grid(row = 1,column = 1)
window.mainloop()
#functions