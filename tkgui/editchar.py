# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 11:22:58 2019

@author: pjshzs
"""

import tkinter as tk   # AFAIK Tkinter is always capitalized
import os
#import easygui as eg
class ScrollableFrame(tk.Frame):
    def __init__(self, master, **kwargs):
        tk.Frame.__init__(self, master, **kwargs)

        # create a canvas object and a vertical scrollbar for scrolling it
        self.vscrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        self.vscrollbar.pack(side='right', fill="y",  expand="false")
        self.canvas = tk.Canvas(self,
                                bg='#444444', bd=0,
                                height=350,
                                highlightthickness=0,
                                yscrollcommand=self.vscrollbar.set)
        self.canvas.pack(side="left", fill="both", expand="true")
        self.vscrollbar.config(command=self.canvas.yview)

        # reset the view
        self.canvas.xview_moveto(0)
        self.canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = tk.Frame(self.canvas, **kwargs)
        self.canvas.create_window(0, 0, window=self.interior, anchor="nw")

        self.bind('<Configure>', self.set_scrollregion)


    def set_scrollregion(self, event=None):
        """ Set the scroll region on the canvas"""
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))
        
def popup_bonus():
    win = tk.Toplevel()
    win.wm_title("Window")

    items=app.items
    checkbox_pane = ScrollableFrame(win, bg='#444444')
    checkbox_pane.pack(expand="true", fill="both")
    i=0
    for name in items:
        i+=1
        tk.Checkbutton(checkbox_pane.interior, text=name).grid(row=i, column=0)
class App(tk.Frame):
    characterPrefix = "character_"
    def __init__(self, master):
        tk.Frame.__init__(self, master)

        # character box
        tk.Label(self, text = "Characters Editor").grid(row = 0, column = 0, rowspan = 1, columnspan = 2)
        self.charbox = tk.Listbox(self)  # You'll want to keep this reference as an attribute of the class too.
        self.items=[('ketchuppp'),('moyyyo')]
        for chars in self.items:
            self.charbox.insert(END, chars)
        self.charbox.grid(row = 1, column = 0, rowspan = 5)
        charadd = tk.Button(self, text = "   Add   ", command = self.addchar).grid(row = 1, column = 1)
        charremove = tk.Button(self, text = "Remove", command = self.removechar).grid(row = 2, column = 1)
        chooselib=tk.Button(self,text="Library", command=popup_bonus).grid(row = 3, column = 1)
       
    def addchar(self, initialCharacter='', initialInfo=''):
        t = tk.Toplevel(root)  # Creates a new window
        t.title("Add character")
        characterLabel = tk.Label(t, text="Character name:")
        characterEntry = tk.Entry(t)
        characterEntry.insert(0, initialCharacter)
        infoLabel = tk.Label(t, text="Info:")
        infoEntry = tk.Entry(t)
        infoEntry.insert(0, initialInfo)
        def create():
            characterName = characterEntry.get()
            self.charbox.insert(END, characterName)
            self.items.append(characterName)
        #    with open(app.characterPrefix + characterName, 'w') as f:
        #            f.write(infoEntry.get())
            t.destroy()
        createButton = tk.Button(t, text="Create", command=create)
        cancelButton = tk.Button(t, text="Cancel", command=t.destroy)

        characterLabel.grid(row=0, column=0)
        infoLabel.grid(row=0, column=1)
        characterEntry.grid(row=1, column=0)
        infoEntry.grid(row=1, column=1)
        createButton.grid(row=2, column=0)
        cancelButton.grid(row=2, column=1)

    def removechar(self):
        for index in self.charbox.curselection():
            item = self.charbox.get(int(index))
            self.charbox.delete(int(index))
            self.items.remove(self.items[int(index)])

   # def editchar(self):
        # You can implement this one ;)

root = tk.Tk()
root.wm_title("IA Development Kit")
app = App(root)
app.pack(expand=True, fill='both')
root.mainloop()