# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 14:20:45 2019

@author: pjshzs
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from skimage.color import rgb2gray
img = cv2.imread('road3.jpg')
pic = plt.imread('road5.jpg')/255  # dividing by 255 to bring the pixel values between 0 and 1
#print(pic.shape)
#plt.imshow(pic)
pic_n = pic.reshape(pic.shape[0]*pic.shape[1], pic.shape[2])
#pic_n.shape
kmeans = KMeans(n_clusters=5, random_state=0).fit(pic_n)
pic2show = kmeans.cluster_centers_[kmeans.labels_]
cluster_pic = pic2show.reshape(pic.shape[0], pic.shape[1], pic.shape[2])
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray2=rgb2gray(cluster_pic)
gray2=np.int32(gray2*254)
print(gray2)
print(gray)
#ret,bw = cv2.threshold(gray,127,255,cv2.THRESH_BINARY)
#edges1 = cv2.Canny(bw,50,150,apertureSize = 3)
edges = cv2.Canny(gray2,150,200,apertureSize = 3)
edges2= cv2.Canny(gray,500,600)
minLineLength = 30
maxLineGap = 5

lines = cv2.HoughLinesP(edges,cv2.HOUGH_PROBABILISTIC, np.pi/180, 30, minLineLength,maxLineGap)
#print(np.size(lines,0))
for num in range(10):

    for x1,y1,x2,y2 in lines[num]:
    
        cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)

#cv2.imwrite('houghlines7.png',img)
plt.subplot(121),plt.imshow(gray),plt.title('Input')
plt.subplot(122),plt.imshow(img),plt.title('Output')
plt.show()
# %matplotlib qt