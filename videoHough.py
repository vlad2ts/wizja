# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 12:39:42 2019

@author: pjshzs
"""

import numpy as np
import cv2
import time

cap = cv2.VideoCapture('Freeway.mp4')
lines=[2,13,3]
while(cap.isOpened()):
    ret, img = cap.read()
    bet, img_copy = cap.read()
    Ysize=int(np.size(img,0))
    Xsize=int(np.size(img,1))
    Yhalf=int(Ysize/1.5)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret,bw = cv2.threshold(gray,127,255,cv2.THRESH_BINARY)
    edges = cv2.Canny(bw,50,150,apertureSize = 3)
    lines = cv2.HoughLines(edges,1,np.pi/180,100)
    lineLength=1000
    if  np.any(lines):
    
        for num in range(5):
        
            for rho,theta in lines[num]:    
            
                a = np.cos(theta)
            
                b = np.sin(theta)
                x0 = a*rho
                y0 = b*rho
                x1 = int(x0 + lineLength*(-b))
                y1 = int(y0 + lineLength*(a))
                x2 = int(x0 - lineLength*(-b))
                y2 = int(y0 - lineLength*(a))
            
                cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
        
        img_crop = img[Yhalf:Ysize,0:Xsize]

        img_copy[Yhalf:Ysize,0:Xsize]=img_crop

        cv2.imshow('frame',img_copy)
    time.sleep(1/30)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()